package tn.irmc.esprit.entities;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.naming.NamingException;

@Stateless
@LocalBean
public class sendmail implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Resource(name = "java:jboss/mail/Gmail")
	private Session session;

	public  void sendMail(String content ,String reciever, String subject) throws AddressException, MessagingException, NamingException{
	Message message = new MimeMessage(session);
    message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(reciever));
	message.setSubject(subject);
	message.setText(content);
	Transport.send(message);
    }

}
