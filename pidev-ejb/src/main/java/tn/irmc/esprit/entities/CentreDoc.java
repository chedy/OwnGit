package tn.irmc.esprit.entities;

import java.io.Serializable; 
import java.util.Date;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: CentreDoc
 *
 */
@Entity

public class CentreDoc implements Serializable {

	   
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "cen_id", updatable = false, nullable = false)
	private int cen_id;
	@ManyToOne
	private User user;
	@OneToOne(fetch = FetchType.EAGER, mappedBy = "centredoc", cascade = CascadeType.ALL)
	private Localisation localisation;	
	private String cen_type		;	
	private String cen_nom			;	
	private String cen_sigle		;	
	private String cen_siteweb_url	;		
	private String cen_facebook_url;		
	private String cen_contact_email;		
	private int cen_contact_telephone;		
	private String cen_horaires		;	
	private String cen_acces_type		;	
	private String cen_image_url		;	
	private String cen_catalogue_url	;	
	private String cen_internet_type	;	
	private Date cen_lastchange_time;  			
	private String cen_lastchange_by;		
	private String cen_commentaire;			
	private String cen_pret;			
	private String Type	;			
	private String Domaines; 
	private int confirmed =0;
	private int recommandation = 0;
	private static final long serialVersionUID = 1L;

	public CentreDoc() {
		super();
	}   
	
	
	
	
	















	public CentreDoc(User user, Localisation localisation, String cen_type, String cen_nom) {
		super();
		this.user = user;
		this.localisation = localisation;
		this.cen_type = cen_type;
		this.cen_nom = cen_nom;
	}




















	public CentreDoc(User user, Localisation localisation, String cen_type, String cen_nom, String cen_sigle,
			String cen_siteweb_url, String cen_facebook_url, String cen_contact_email, int cen_contact_telephone,
			String cen_horaires, String cen_acces_type, String cen_image_url, String cen_catalogue_url,
			String cen_internet_type, Date cen_lastchange_time, String cen_lastchange_by, String cen_commentaire,
			String cen_pret, String type, String domaines, int confirmed) {
		super();
		this.user = user;
		
		this.cen_type = cen_type;
		this.cen_nom = cen_nom;
		this.cen_sigle = cen_sigle;
		this.cen_siteweb_url = cen_siteweb_url;
		this.cen_facebook_url = cen_facebook_url;
		this.cen_contact_email = cen_contact_email;
		this.cen_contact_telephone = cen_contact_telephone;
		this.cen_horaires = cen_horaires;
		this.cen_acces_type = cen_acces_type;
		this.cen_image_url = cen_image_url;
		this.cen_catalogue_url = cen_catalogue_url;
		this.cen_internet_type = cen_internet_type;
		this.cen_lastchange_time = cen_lastchange_time;
		this.cen_lastchange_by = cen_lastchange_by;
		this.cen_commentaire = cen_commentaire;
		this.cen_pret = cen_pret;
		Type = type;
		Domaines = domaines;
		this.confirmed = confirmed;
	}




















	public CentreDoc(User user, Localisation localisation, String cen_nom, int recommandation) {
		super();
		this.user = user;
		this.localisation = localisation;
		this.cen_nom = cen_nom;
		this.recommandation = recommandation;
	}




















	public int getCen_id() {
		return this.cen_id;
	}

	public void setCen_id(int cen_id) {
		this.cen_id = cen_id;
	}
	
	public String getCen_type() {
		return cen_type;
	}
	public void setCen_type(String cen_type) {
		this.cen_type = cen_type;
	}
	public String getCen_nom() {
		return cen_nom;
	}
	public void setCen_nom(String cen_nom) {
		this.cen_nom = cen_nom;
	}
	public String getCen_sigle() {
		return cen_sigle;
	}
	public void setCen_sigle(String cen_sigle) {
		this.cen_sigle = cen_sigle;
	}
	

	
	
	public String getCen_siteweb_url() {
		return cen_siteweb_url;
	}
	public void setCen_siteweb_url(String cen_siteweb_url) {
		this.cen_siteweb_url = cen_siteweb_url;
	}
	public String getCen_facebook_url() {
		return cen_facebook_url;
	}
	public void setCen_facebook_url(String cen_facebook_url) {
		this.cen_facebook_url = cen_facebook_url;
	}
	public String getCen_contact_email() {
		return cen_contact_email;
	}
	public void setCen_contact_email(String cen_contact_email) {
		this.cen_contact_email = cen_contact_email;
	}
	public int getCen_contact_telephone() {
		return cen_contact_telephone;
	}
	public void setCen_contact_telephone(int cen_contact_telephone) {
		this.cen_contact_telephone = cen_contact_telephone;
	}
	public String getCen_horaires() {
		return cen_horaires;
	}
	public void setCen_horaires(String cen_horaires) {
		this.cen_horaires = cen_horaires;
	}
	public String getCen_acces_type() {
		return cen_acces_type;
	}
	public void setCen_acces_type(String cen_acces_type) {
		this.cen_acces_type = cen_acces_type;
	}
	public String getCen_image_url() {
		return cen_image_url;
	}
	public void setCen_image_url(String cen_image_url) {
		this.cen_image_url = cen_image_url;
	}
	public String getCen_catalogue_url() {
		return cen_catalogue_url;
	}
	public void setCen_catalogue_url(String cen_catalogue_url) {
		this.cen_catalogue_url = cen_catalogue_url;
	}
	public String getCen_internet_type() {
		return cen_internet_type;
	}
	public void setCen_internet_type(String cen_internet_type) {
		this.cen_internet_type = cen_internet_type;
	}
	public Date getCen_lastchange_time() {
		return cen_lastchange_time;
	}
	public void setCen_lastchange_time(Date cen_lastchange_time) {
		this.cen_lastchange_time = cen_lastchange_time;
	}
	public String getCen_lastchange_by() {
		return cen_lastchange_by;
	}
	public void setCen_lastchange_by(String cen_lastchange_by) {
		this.cen_lastchange_by = cen_lastchange_by;
	}
	public String getCen_commentaire() {
		return cen_commentaire;
	}
	public void setCen_commentaire(String cen_commentaire) {
		this.cen_commentaire = cen_commentaire;
	}
	public String getCen_pret() {
		return cen_pret;
	}
	public void setCen_pret(String cen_pret) {
		this.cen_pret = cen_pret;
	}

	
	public String getType() {
		return Type;
	}
	public void setType(String type) {
		Type = type;
	}
	public String getDomaines() {
		return Domaines;
	}
	public void setDomaines(String domaines) {
		Domaines = domaines;
	}







	public int getConfirmed() {
		return confirmed;
	}







	public void setConfirmed(int confirmed) {
		this.confirmed = confirmed;
	}












	public int getRecommandation() {
		return recommandation;
	}




















	public void setRecommandation(int recommandation) {
		this.recommandation = recommandation;
	}




















	public Localisation getLocalisation() {
		return localisation;
	}




















	public void setLocalisation(Localisation localisation) {
		this.localisation = localisation;
	}




















	public User getUser() {
		return user;
	}


	public void setUser(User user) {
		this.user = user;
	}
   
}

