package tn.irmc.esprit.entities;

import java.io.Serializable;
import java.lang.String;
import java.sql.Time;
import java.util.Date;
import javax.persistence.*;

/**
 * Entity implementation class for Entity: Evenement
 *
 */
@Entity

public class Evenement implements Serializable {

	@GeneratedValue(strategy=GenerationType.IDENTITY)   
	@Id
	private int idEvent;
	@ManyToOne(cascade = CascadeType.MERGE)
	private User user;
	private String Nom;
	private String Description;
	private int Tel;
	private int NbrPlace;
	private float Prix;
	private String Lieux;
	@Temporal(TemporalType.DATE)
	private Date DateDebut;
	@Temporal(TemporalType.DATE)
	private Date DateFin;
	private String Map;
	private int NbrPlaceReser;
	private Time HeureDepart;
	private static final long serialVersionUID = 1L;
	private int NbrSignal;
	private String type;

	
	
	

	  
	public Evenement(User user, String nom, String description, int tel, int nbrPlace, Float prix, String lieux,
			Date dateDebut, Date dateFin, String map, int nbrPlaceReser, Time heureDepart, int nbrSignal, String type) {
		super();
		this.user = user;
		Nom = nom;
		Description = description;
		Tel = tel;
		NbrPlace = nbrPlace;
		Prix = prix;
		Lieux = lieux;
		DateDebut = dateDebut;
		DateFin = dateFin;
		Map = map;
		NbrPlaceReser = nbrPlaceReser;
		HeureDepart = heureDepart;
		NbrSignal = nbrSignal;
		this.type = type;
	}
	

	public Evenement(String nom, String description, int tel, int nbrPlace, Float prix, String lieux, Date dateDebut,
			Date dateFin, String map, int nbrPlaceReser, Time heureDepart, int nbrSignal, String type) {
		super();
		Nom = nom;
		Description = description;
		Tel = tel;
		NbrPlace = nbrPlace;
		Prix = prix;
		Lieux = lieux;
		DateDebut = dateDebut;
		DateFin = dateFin;
		Map = map;
		NbrPlaceReser = nbrPlaceReser;
		HeureDepart = heureDepart;
		NbrSignal = nbrSignal;
		this.type = type;
	}


	public int getIdEvent() {
		return this.idEvent;
	}

	public void setIdEvent(int idEvent) {
		this.idEvent = idEvent;
	}   
	public String getNom() {
		return this.Nom;
	}

	public void setNom(String Nom) {
		this.Nom = Nom;
	}   
	public String getDescription() {
		return this.Description;
	}

	public void setDescription(String Description) {
		this.Description = Description;
	}   
	public int getTel() {
		return this.Tel;
	}

	public void setTel(int Tel) {
		this.Tel = Tel;
	}   
	public int getNbrPlace() {
		return this.NbrPlace;
	}

	public void setNbrPlace(int NbrPlace) {
		this.NbrPlace = NbrPlace;
	}   

	


	public float getPrix() {
		return Prix;
	}


	public void setPrix(float prix) {
		Prix = prix;
	}


	public String getLieux() {
		return this.Lieux;
	}

	public void setLieux(String Lieux) {
		this.Lieux = Lieux;
	}   
	
	
	
	public String getMap() {
		return this.Map;
	}

	public void setMap(String Map) {
		this.Map = Map;
	}   
	public int getNbrPlaceReser() {
		return this.NbrPlaceReser;
	}

	public void setNbrPlaceReser(int NbrPlaceReser) {
		this.NbrPlaceReser = NbrPlaceReser;
	}
	public Time getHeureDepart() {
		return HeureDepart;
	}

	public void setHeureDepart(Time heureDepart) {
		HeureDepart = heureDepart;
	} 
	

	
	public Date getDateDebut() {
		return DateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		DateDebut = dateDebut;
	}

	public Date getDateFin() {
		return DateFin;
	}

	public void setDateFin(Date dateFin) {
		DateFin = dateFin;
	}

	public Evenement() {
		super();
	}

	public int getNbrSignal() {
		return NbrSignal;
	}

	public void setNbrSignal(int nbrSignal) {
		NbrSignal = nbrSignal;
	}

	

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	
	@Override
	public String toString() {
		return "Evenement [idEvent=" + idEvent + ", Nom=" + Nom + ", Description=" + Description + ", Tel=" + Tel
				+ ", NbrPlace=" + NbrPlace + ", Prix=" + Prix + ", Lieux=" + Lieux + ", DateDebut=" + DateDebut
				+ ", DateFin=" + DateFin + ", Map=" + Map + ", NbrPlaceReser=" + NbrPlaceReser + "]";
	}
	
	
   
}
