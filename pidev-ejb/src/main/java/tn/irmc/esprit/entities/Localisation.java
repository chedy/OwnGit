package tn.irmc.esprit.entities;

import java.io.Serializable; 
import java.util.Date;
import javax.persistence.*;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity

public class Localisation implements Serializable {

	   
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "locid", updatable = false, nullable = false)
	private int locID;
	@OneToOne(fetch = FetchType.EAGER)
	private CentreDoc centredoc;
	private String cen_pays		;	
	private String cen_adresse		;	
	private int cen_codepostal	;		
	private String cen_ville		;	
	private float longitude			;	
	private float latitude;
	private String Pays	;			
	
	private static final long serialVersionUID = 1L;

	public Localisation() {
		super();
	}

	public Localisation(String cen_pays, int cen_codepostal) {
		super();
		this.cen_pays = cen_pays;
		this.cen_codepostal = cen_codepostal;
	}


	public Localisation(CentreDoc centredoc, String cen_pays, String cen_adresse, int cen_codepostal, String cen_ville,
			int longitude, int latitude, String pays) {
		super();
		this.centredoc = centredoc;
		this.cen_pays = cen_pays;
		this.cen_adresse = cen_adresse;
		this.cen_codepostal = cen_codepostal;
		this.cen_ville = cen_ville;
		this.longitude = longitude;
		this.latitude = latitude;
		Pays = pays;
	}



	public int getLocID() {
		return locID;
	}

	public void setLocID(int locID) {
		this.locID = locID;
	}

	public String getCen_pays() {
		return cen_pays;
	}

	public void setCen_pays(String cen_pays) {
		this.cen_pays = cen_pays;
	}

	public String getCen_adresse() {
		return cen_adresse;
	}

	public void setCen_adresse(String cen_adresse) {
		this.cen_adresse = cen_adresse;
	}

	public int getCen_codepostal() {
		return cen_codepostal;
	}

	public void setCen_codepostal(int cen_codepostal) {
		this.cen_codepostal = cen_codepostal;
	}

	public String getCen_ville() {
		return cen_ville;
	}

	public void setCen_ville(String cen_ville) {
		this.cen_ville = cen_ville;
	}


	public String getPays() {
		return Pays;
	}

	public void setPays(String pays) {
		Pays = pays;
	}


	public float getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}

	public float getLatitude() {
		return latitude;
	}

	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}

	public CentreDoc getCentredoc() {
		return centredoc;
	}

	public void setCentredoc(CentreDoc centredoc) {
		this.centredoc = centredoc;
	}   
	
	
	
	
	


}

