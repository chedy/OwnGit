package tn.irmc.esprit.services;

import java.util.List;

import javax.ejb.Remote;

import tn.irmc.esprit.entities.Offre;
import tn.irmc.esprit.entities.Reclamation;

@Remote
public interface ReclamationServiceRemote {
	public void addReclamation(Reclamation item);
	public void deleteReclamation(Reclamation item);
	public Reclamation findById(int idItem);
	public List<Reclamation> findAllRec();
}
