package tn.irmc.esprit.services;

import java.sql.Time;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;


import tn.irmc.esprit.entities.CurrentUser;
import tn.irmc.esprit.entities.Evenement;
import tn.irmc.esprit.entities.User;

/**
 * Session Bean implementation class CRUDevent
 */
@Stateless
@LocalBean
public class EvenementService implements EvenementServiceLocal,EvenementServiceRemote {
	
	@PersistenceContext
	private EntityManager em;
	
	UserServiceLocal serviceUserServiceLocal;
	
public static int ESL;

    /**
     * Default constructor. 
     */
    public EvenementService() {
        // TODO Auto-generated constructor stub
    }
    
   

    
    @Override
	public void addEvent(Evenement evenement) {
		// TODO Auto-generated method stub
    	evenement.setUser(findByIdUser(CurrentUser.CurrentUserId));	
	    em.persist(evenement);
	}
    @Override
	public User findByIdUser(int id) {
		// TODO Auto-generated method stub
		return em.find(User.class, id);
	}
	

	@Override
	public void updateEvent(Evenement evenement) {
		em.merge(evenement);
		
	}

	@Override
	public void removeEvent(Evenement evenement) {
		// TODO Auto-generated method stub
		
		em.remove(em.merge(evenement));
		
	}

	@Override
	public List<Evenement> findAllEvents() {
		// TODO Auto-generated method stub
		return em.createQuery("select e from Evenement e", Evenement.class)
				.getResultList();
	}

	@Override
	public Evenement findById(int idEvent) {
		// TODO Auto-generated method stub
		
		 return em.find(Evenement.class, idEvent);
	}

	@Override
	public Evenement findByName(String nom) {
		// TODO Auto-generated method stub
		Evenement found = null;
		TypedQuery<Evenement> query = em.createQuery(
				"select e from Evenement e where e.Nom=:x", Evenement.class);
		query.setParameter("x", nom);
		try {
			found = query.getSingleResult();
		} catch (Exception ex) {
			Logger.getLogger(this.getClass().getName()).log(Level.INFO,
					"no Evenement with name=" + nom);
		}
		return found;
	}


	




	







	

	

	

	








	




	


	
}

