package tn.irmc.esprit.services;

import java.util.List;

import javax.ejb.Local;

import tn.irmc.esprit.entities.Offre;
import tn.irmc.esprit.entities.Reclamation;

@Local
public interface ReclamationServiceLocal {
	public void addReclamation(Reclamation item);
	public void deleteReclamation(Reclamation item);
	public Reclamation findById(int idItem);
	public List<Reclamation> findAllRec();
}
