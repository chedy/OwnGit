package tn.irmc.esprit.services;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.management.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import tn.irmc.esprit.entities.CurrentUser;
import tn.irmc.esprit.entities.Offre;
import tn.irmc.esprit.entities.User;

/**
 * Session Bean implementation class OfferService
 */
@Stateless
@LocalBean
public class OfferService implements OfferServiceRemote, OfferServiceLocal {

	/**
	 * Default constructor.
	 */
	public OfferService() {
		// TODO Auto-generated constructor stub
	}

	@PersistenceContext
	private EntityManager em;
	UserServiceRemote serviceuser;

	@Override
	public void updateOffre(Offre offre) {
		// TODO Auto-generated method stub
		em.merge(offre);
	}

	@Override
	public void deleteOffre(Offre offre) {
		// TODO Auto-generated method stub
		em.remove(em.merge(offre));
	}

	@Override
	public Offre findById(int idoffre) {
		// TODO Auto-generated method stub
		return em.find(Offre.class, idoffre);
	}

	@Override
	public Offre findByName(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public User findByIdUser(int id) {
		// TODO Auto-generated method stub
		return em.find(User.class, id);
	}

	@Override
	public void addoffre(Offre o) {
		o.setUser(findByIdUser(CurrentUser.CurrentUserId));
		em.persist(o);
	}

	@Override
	public void addBourse(Offre o) {
		// TODO Auto-generated method stub
		o.setUser(findByIdUser(CurrentUser.CurrentUserId));
		em.persist(o);
	}

	@Override
	public void updateBourse(Offre item) {
		// TODO Auto-generated method stub
		em.merge(item);

	}

	@Override
	public void deleteBourse(Offre item) {
		// TODO Auto-generated method stub
		em.remove(em.merge(item));
	}

	// liste emploie kemlaaaa
	@Override
	public List<Offre> findAllOffers() {
		// TODO Auto-generated method stub
		return em.createQuery("SELECT i FROM Offre i WHERE TypeOffre = 'emploi' and DateFin > sysdate() AND etat = 1 ", Offre.class)
				.getResultList();
	}

	// list bourse kemlaaaa
	@Override
	public List<Offre> findAllBourse() {
		// TODO Auto-generated method stub
		return em.createQuery("SELECT i FROM Offre i WHERE TypeOffre = 'Bourse' and DateFin > sysdate() AND etat = 1 ", Offre.class)
				.getResultList();
	}

	// pour admin suppression
	@Override
	public List<Offre> findAllOffreExpiré() {
		// TODO Auto-generated method stub
		return em.createQuery("SELECT i FROM Offre i WHERE DateFin < sysdate() ", Offre.class).getResultList();
	}

	// les emploie te3i lkol
	@Override
	public List<Offre> findMyAllOffersAttente() {
		// TODO Auto-generated method stub
		return em.createQuery(
				"SELECT i FROM Offre i WHERE user_id = " + CurrentUser.CurrentUserId + " AND TypeOffre = 'emploi' AND etat = 0 ",
				Offre.class).getResultList();
	}

	// les bourses te3i lkol
	@Override
	public List<Offre> findMyAllBourseAttente() {
		// TODO Auto-generated method stub
		return em.createQuery(
				"SELECT i FROM Offre i WHERE user_id = " + CurrentUser.CurrentUserId + " AND TypeOffre = 'Bourse' AND etat = 0 ",
				Offre.class).getResultList();
	}

	// mes publications en attente
	@Override
	public List<Offre> findMyAllAttente() {
		// TODO Auto-generated method stub
		return em.createQuery("SELECT i FROM Offre i WHERE user_id = " + CurrentUser.CurrentUserId + " AND etat = 0 ",
				Offre.class).getResultList();
	}

	// les offres te3i publier
	@Override
	public List<Offre> findMyOffrePub() {
		// TODO Auto-generated method stub
		return em.createQuery("SELECT i FROM Offre i WHERE user_id = " + CurrentUser.CurrentUserId
				+ " AND TypeOffre = 'emploi' AND etat = 1 ", Offre.class).getResultList();
	}

	// les bourses te3i publier
	@Override
	public List<Offre> findMyBoursePub() {
		// TODO Auto-generated method stub
		return em.createQuery("SELECT i FROM Offre i WHERE user_id = " + CurrentUser.CurrentUserId
				+ " AND TypeOffre = 'Bourse' AND etat = 1 ", Offre.class).getResultList();
	}

	// pour admin
	@Override
	public List<Offre> findAllOffreAttente() {
		// TODO Auto-generated method stub
		return em.createQuery("SELECT i FROM Offre i WHERE etat = 0 AND TypeOffre = 'emploi' and DateFin > sysdate() ", Offre.class)
				.getResultList();
	}

	// pour admin
	@Override
	public List<Offre> findAllbourseAttente() {
		// TODO Auto-generated method stub
		return em.createQuery("SELECT i FROM Offre i WHERE etat = 0 AND TypeOffre = 'Bourse' and DateFin > sysdate() ", Offre.class)
				.getResultList();
	}

	@Override
	public void deleteAttente() {
		// TODO Auto-generated method stub
		javax.persistence.Query query= em.createQuery("delete FROM Offre i where DateFin < sysdate() ");
		query.executeUpdate();

	}
	@Override
	public void AccepteAttente(int id) {
		// TODO Auto-generated method stub
		javax.persistence.Query query= em.createQuery("UPDATE Offre i SET etat = 1 WHERE id = "+id);
		query.executeUpdate();
	}
	@Override
	public void RefuserAttente(int id) {
		// TODO Auto-generated method stub
		javax.persistence.Query query= em.createQuery("DELETE FROM Offre WHERE id = "+id);
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Offre> RechercheOffre(String title , String lieu) {
		// TODO Auto-generated method stub
		if(title != null && lieu != null)
		{
			String jpql = "SELECT o FROM Offre o WHERE o.Titre like :param and o.Mobilite like :parame and etat = 1 and TypeOffre = 'emploi' and DateFin > sysdate()  ";
			javax.persistence.Query query =   em.createQuery(jpql);
			query.setParameter("param", "%"+title+"%");
			query.setParameter("parame", "%"+lieu+"%");
			return query.getResultList();
		}
		if(title != null)
		{
			String jpql = "SELECT o FROM Offre o WHERE o.Titre like :param and etat = 1 and TypeOffre = 'emploi' and DateFin > sysdate()  ";
			javax.persistence.Query query =   em.createQuery(jpql);
			query.setParameter("param", "%"+title+"%");
			return query.getResultList();
		}
		if(lieu != null)
		{
			String jpql = "SELECT o FROM Offre o WHERE o.Mobilite like :param and etat = 1 and TypeOffre = 'emploi' and DateFin > sysdate()  ";
			javax.persistence.Query query =   em.createQuery(jpql);
			query.setParameter("param", "%"+lieu+"%");
			return query.getResultList();
		}
		return null;
	}

	@Override
	public List<Offre> RechercheBourse(String title, String type) {
		// TODO Auto-generated method stub
		if(title != null && type != null)
		{
			String jpql = "SELECT o FROM Offre o WHERE o.Titre like :param and o.TypePoste like :parame and etat = 1 and TypeOffre = 'Bourse' and DateFin > sysdate()  ";
			javax.persistence.Query query =   em.createQuery(jpql);
			query.setParameter("param", "%"+title+"%");
			query.setParameter("parame", "%"+type+"%");
			return query.getResultList();
		}
		
		if(title != null)
		{
			String jpql = "SELECT o FROM Offre o WHERE o.Titre like :param and etat = 1 and TypeOffre = 'Bourse' and DateFin > sysdate()  ";
			javax.persistence.Query query =   em.createQuery(jpql);
			query.setParameter("param", "%"+title+"%");
			return query.getResultList();
		}
		if(type != null)
		{
			String jpql = "SELECT o FROM Offre o WHERE o.TypePoste like :param and etat = 1 and TypeOffre = 'Bourse' and DateFin > sysdate()  ";
			javax.persistence.Query query =   em.createQuery(jpql);
			query.setParameter("param", "%"+type+"%");
			return query.getResultList();
		}
		
		return null;
		
	}
}