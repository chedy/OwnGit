package tn.irmc.esprit.services;

import java.util.List;  

import javax.ejb.Local;

import tn.irmc.esprit.entities.CentreDoc;
import tn.irmc.esprit.entities.Localisation;
import tn.irmc.esprit.entities.User;


@Local
public interface CentreServiceLocal {
	void addCentre(CentreDoc centre, Localisation local);
	void DeleteCentre(CentreDoc centre, Localisation local,int cenID);
    void UpdateCentre(CentreDoc centre);
    public CentreDoc findById(int idCentre);
	public List<CentreDoc> findAllCentreDocs();
	public CentreDoc findByName(String name);
   void confirmCentre (CentreDoc centre);
   public List<CentreDoc> findConfirmedCentre();
	public List<CentreDoc> getCentres();
	User findByIdUser(int id);
	public List<CentreDoc> findByUserCountry ();
	List<CentreDoc> RechercheCentre(String nomSigle, String pays, String ville, String type , int recom);
	
	
	
	

	
}
