package tn.irmc.esprit.services;

import java.util.List;

import javax.ejb.Remote;

import tn.irmc.esprit.entities.User;

@Remote
public interface UserServiceRemote {
	
void createUser(User user);
	
	List<User> findAllUsers();
	User authenticate(String login, String password);
	boolean loginExists(String login);
	User findUserByLogin(String login);
	void UpdateUser(User user);
	void DeleteUser(User user);
	User findById(int iduser);

}
