package tn.irmc.esprit.ManagedBeans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import tn.irmc.esprit.entities.CurrentUser;
import tn.irmc.esprit.entities.Role;
import tn.irmc.esprit.entities.User;
import tn.irmc.esprit.services.UserServiceLocal;

@ManagedBean(name = "regBean")
@RequestScoped
public class RegisterBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@ManagedProperty("#{authBean}")
	private AuthenticationBean authBean;
	@EJB
	private UserServiceLocal userService;
	private User user;
	private boolean loggedIn;
	private static CurrentUser LoggedUser;
	
	
	public RegisterBean() {
		super();
	}

	@PostConstruct
	public void init() {
		user = new User();
	}

	public String doSignUp() {
		String navigateTo = null;
		user.setRole(Role.User);
		userService.UpdateUser(user);
		authBean.setUser(user);
		return authBean.doLogin();
	}
	
	public void validateLoginUnicity(FacesContext context,
			UIComponent component, Object value) throws ValidatorException {
		String loginToValidate = (String) value;
		if (loginToValidate == null || loginToValidate.trim().isEmpty()) {
			return;
		}
		boolean loginInUse = userService.loginExists(loginToValidate);
		if (loginInUse) {
			throw new ValidatorException(new FacesMessage(
					"login already in use!"));
		}
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public AuthenticationBean getAuthBean() {
		return authBean;
	}

	public void setAuthBean(AuthenticationBean authBean) {
		this.authBean = authBean;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public static CurrentUser getLoggedUser() {
		return LoggedUser;
	}

	public static void setLoggedUser(CurrentUser loggedUser) {
		LoggedUser = loggedUser;
	}

	
	
	
	

}
