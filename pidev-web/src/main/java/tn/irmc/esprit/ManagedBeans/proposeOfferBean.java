package tn.irmc.esprit.ManagedBeans;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import tn.irmc.esprit.entities.CurrentUser;
import tn.irmc.esprit.entities.Offre;
import tn.irmc.esprit.services.OfferServiceLocal;

@ManagedBean(name = "offerBean")
@RequestScoped
public class proposeOfferBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EJB
	private OfferServiceLocal serviceOffer;
	private Offre o;
	private List<Offre> offres = new ArrayList<>();
	private List<Offre> bourses = new ArrayList<>();
	private List<Offre> myoffresattente = new ArrayList<>();
	private List<Offre> mybourseattente = new ArrayList<>();
	private List<Offre> myoffrespub = new ArrayList<>();
	private List<Offre> myboursespub = new ArrayList<>();
	private List<Offre> offreExpiré = new ArrayList<>();
	private List<Offre> offresAttente = new ArrayList<>();
	private List<Offre> boursesAttente = new ArrayList<>();
	private List<Offre> recherche = new ArrayList<>();
	private List<Offre> rechercheBourse = new ArrayList<>();
	public static int id;
	private static String lieu;
	private static String title;
	private boolean showDetails = false;
	private Date date;
    
	@PostConstruct
	public void initModel() {
		o = new Offre();
		offres = serviceOffer.findAllOffers();
		bourses = serviceOffer.findAllBourse();
		myoffresattente = serviceOffer.findMyAllOffersAttente();
		mybourseattente = serviceOffer.findMyAllBourseAttente();
		myoffrespub = serviceOffer.findMyOffrePub();
		myboursespub = serviceOffer.findMyBoursePub();
		offreExpiré = serviceOffer.findAllOffreExpiré();
		offresAttente = serviceOffer.findAllOffreAttente();
		boursesAttente = serviceOffer.findAllbourseAttente();
		recherche =serviceOffer.RechercheOffre(title, lieu);
		rechercheBourse =serviceOffer.RechercheBourse(title,lieu);
		date = new Date();
	}

	public String doAddoffer() throws ParseException {
		String navTo = "/pages/Home?faces-redirect=true";
		o.setTypeOffre("emploi");
		o.setDatePublication(date);
		o.setEtat(false);
		serviceOffer.addoffre(o);
		initModel();
		return navTo;
	}
	
	public String doAddBourse() {
		String navTo = "/pages/Home?faces-redirect=true";
		o.setDatePublication(date);
		o.setTypeOffre("bourse");
		o.setEtat(false);
		serviceOffer.addoffre(o);
		initModel();
		return navTo;
	}

	public String dodeleteffer() {
		String navTo = "/pages/Home?faces-redirect=true";
		serviceOffer.deleteOffre(o);
		initModel();
		return navTo;
	}
	public String dodeleteAll() {
		String navTo = "/pages/Home?faces-redirect=true";
		serviceOffer.deleteAttente();
		initModel();
		return navTo;
	}
	public String dodeletBourse() {
		String navTo = "/pages/Home?faces-redirect=true";
		serviceOffer.deleteBourse(o);
		initModel();
		return navTo;
	}

	public String editBourse(Offre e) {
		this.o = e;
		return "editBourse";
	}
	

	public String editBourse() {
		this.o.setDatePublication(date);
		this.o.setUser(serviceOffer.findByIdUser(CurrentUser.CurrentUserId));
		this.o.setTypeOffre("bourse");
		this.serviceOffer.updateBourse(this.o);
		return "/pages/Home?faces-redirect=true";
	}

	public String edit(Offre e) {
		this.o = e;
		return "editOffre";
	}
	public String select(Offre e) {
		this.o = e;
		id=o.getId();

		return "details";
	}
	public String edit() {
		this.o.setDatePublication(date);
		this.o.setUser(serviceOffer.findByIdUser(CurrentUser.CurrentUserId));
		this.o.setTypeOffre("emploi");
		this.serviceOffer.updateOffre(this.o);
		return "/pages/Home?faces-redirect=true";
	}

	public void doSelect() {
		showDetails = true;
	}
	
	public String selectAccepter(Offre e) {
		this.o = e;
		id =e.getId();
		return "affichageOffre";
	}
	public String selectAccepterBourse(Offre e) {
		this.o = e;
		id =e.getId();
		return "affichageBourse";
	}
	
	public String doAccepter() {
		this.serviceOffer.AccepteAttente(id);		
     	initModel();
		return "/pages/admin/adminOffre?faces-redirect=true";
	}
	public String doRefuser() {
		this.serviceOffer.RefuserAttente(id); 		
		initModel();
		return "/pages/admin/adminOffre?faces-redirect=true";
	}
	public String doAccepterBourse() {
		this.serviceOffer.AccepteAttente(id);		
		initModel();
		return "/pages/admin/adminBourse?faces-redirect=true";
	}
	public String doRefuserBourse() {
		this.serviceOffer.RefuserAttente(id); 		
		initModel();
		return "/pages/admin/adminBourse?faces-redirect=true";
	}
	public void doCancel() {
		showDetails = false;
	}

	public String doSelectRecheche()
	{
		return "/pages/User/rechecheOffre?faces-redirect=true";

	}
	public String doSelectRechecheBourse()
	{
		return "/pages/User/rechercheBourse?faces-redirect=true";

	}
	public String doselectRec()
	{

		return "/pages/User/reclamation?faces-redirect=true";
	}
	public List<Offre> getBourses() {
		return bourses;
	}

	public void setBourses(List<Offre> bourses) {
		this.bourses = bourses;
	}

	public List<Offre> getMyoffresattente() {
		return myoffresattente;
	}

	public void setMyoffresattente(List<Offre> myoffres) {
		this.myoffresattente = myoffres;
	}
	

	public Offre getO() {
		return o;
	}

	public void setO(Offre o) {
		this.o = o;
	}

	public boolean isShowDetails() {
		return showDetails;
	}

	public void setShowDetails(boolean showDetails) {
		this.showDetails = showDetails;
	}

	public List<Offre> getOffres() {
		return offres;
	}

	public void setOffres(List<Offre> offres) {
		this.offres = offres;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public List<Offre> getMyoffrespub() {
		return myoffrespub;
	}

	public void setMyoffrespub(List<Offre> myoffrespub) {
		this.myoffrespub = myoffrespub;
	}

	public List<Offre> getMyboursespub() {
		return myboursespub;
	}

	public void setMyboursespub(List<Offre> myboursespub) {
		this.myboursespub = myboursespub;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public List<Offre> getOffreExpiré() {
		return offreExpiré;
	}

	public void setOffreExpiré(List<Offre> offreExpiré) {
		this.offreExpiré = offreExpiré;
	}

	public List<Offre> getOffresAttente() {
		return offresAttente;
	}

	public void setOffresAttente(List<Offre> offresAttente) {
		this.offresAttente = offresAttente;
	}

	public List<Offre> getBoursesAttente() {
		return boursesAttente;
	}

	public void setBoursesAttente(List<Offre> boursesAttente) {
		this.boursesAttente = boursesAttente;
	}

	public List<Offre> getMybourseattente() {
		return mybourseattente;
	}

	public void setMybourseattente(List<Offre> mybourseattente) {
		this.mybourseattente = mybourseattente;
	}

	public int getId() {
		return id;
	}

	public void setId(int idd) {
		id = idd;
	}

	public String getLieu() {
		return lieu;
	}

	public void setLieu(String lieuu) {
		lieu = lieuu;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String titlee) {
		title = titlee;
	}

	public List<Offre> getRecherche() {
		return recherche;
	}

	public void setRecherche(List<Offre> recherche) {
		this.recherche = recherche;
	}

	public List<Offre> getRechercheBourse() {
		return rechercheBourse;
	}

	public void setRechercheBourse(List<Offre> rechercheBourse) {
		this.rechercheBourse = rechercheBourse;
	}
}