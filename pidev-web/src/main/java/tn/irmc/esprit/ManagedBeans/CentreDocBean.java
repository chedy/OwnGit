package tn.irmc.esprit.ManagedBeans;

import java.io.Serializable;  
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;

import tn.irmc.esprit.entities.CentreDoc;
import tn.irmc.esprit.entities.Localisation;
import tn.irmc.esprit.services.CentreService;
import tn.irmc.esprit.services.CentreServiceLocal;
import tn.irmc.esprit.services.UserServiceRemote;

import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;

@ManagedBean (name = "cdBean")
//@SessionScoped
@RequestScoped
public class CentreDocBean implements Serializable {

	
	private static final long serialVersionUID = 1L;
	
	@EJB
	private CentreServiceLocal centreServiceLocal;
	
	private CentreDoc centredoc,centredoc1,centredoc2,centre1;
	private Localisation local;
	@EJB
    private UserServiceRemote userService ;
	
	
	
	private List<CentreDoc> centres = new ArrayList<>();
	
	private static String NOMC;
	private static String pays;
	private static String ville;
	private static String type;
	private static int	recom;
	
	private MapModel simpleModel;
	
	public CentreDocBean () {
		
	}
	
	@PostConstruct
	public void init() {
		centredoc = new CentreDoc();
		local = new Localisation();
		centredoc1 = new CentreDoc();
		
		centre1 = new CentreDoc();
		centredoc2=new CentreDoc();
		simpleModel = new DefaultMapModel();
		
		 
		
		
//*****************************************************Crud Bean*******************************************************		
	}
	public String proposeCenter () {
		
		String navTo = "/pages/User/ListCentre2?faces-redirect=true";
		centredoc.setUser(userService.findById(AuthenticationBean.getLoggedUser().CurrentUserId));
		centreServiceLocal.addCentre(centredoc,local);
		return navTo ;
	}
public String declineCenter (int cenID) {
		
		String navTo = "/pages/User/ListCentre?faces-redirect=true";
		centreServiceLocal.DeleteCentre(centredoc,local, cenID);
		return navTo ;
	} 
//*************************************************************************************************************************	****************
//**********************************************Detail du Centre****************************************************************************
	
	public String recup (CentreDoc centredoc1){
		String navTo = "/pages/User/CentreDetails?faces-redirect=true";
		CentreService.IDC = centredoc1.getCen_id();
		System.out.println(centredoc1.getLocalisation().getLatitude());
		System.out.println(centredoc1.getLocalisation().getLatitude());
		return navTo;
		}
	public CentreDoc show (){
		
		centre1 = centreServiceLocal.findById(CentreService.IDC);
		System.out.println(centre1.getLocalisation().getLatitude());
        LatLng coord1 = new LatLng(centre1.getLocalisation().getLatitude(), centre1.getLocalisation().getLongitude());
        simpleModel.addOverlay(new Marker(coord1, centre1.getCen_nom()));
		 return centre1;
	}
//********************************************************Recherche 5 Criteres****************************************************************	
	public String recupNOM (CentreDoc centredoc , Localisation local){
		String navTo = "/pages/User/ListCentre?faces-redirect=true";
		NOMC = centredoc.getCen_nom();
		pays = local.getCen_pays();
		ville = local.getCen_ville();
		type = centredoc.getType();
		recom = centredoc.getRecommandation();
		System.out.println(NOMC);
		System.out.println(pays);
		System.out.println(ville);
		System.out.println(type);
		System.out.println(recom);
		return navTo;
		}	

	public List<CentreDoc> showNom (){
		

		String nom = getNOMC();
		String pays = getPays();
		String ville = getVille();
		String type = getType();
		int rec = getRecom();
		
		
		  
		 return centreServiceLocal.RechercheCentre(nom, pays,ville,type,rec); 
	}
//**********************************************************************************************************************	
//****************************************************Confirmer un Centre***********************************************	

	
	public void confirmCenter (CentreDoc centredoc){
		centredoc.setConfirmed(1);
		centreServiceLocal.UpdateCentre(centredoc);
	}
	
//***********************************************************************************************************************	
//**************************************************List tout les Centres************************************************	
	public  List<CentreDoc> getAll(){
		
		return centreServiceLocal.getCentres();
	}
	
public  List<CentreDoc> getaprox(){
		
		return centreServiceLocal.findByUserCountry();
	}
//***********************************************************************************************************************	
//***********************************************************List Centres Confirmés**************************************	
public  List<CentreDoc> getConfirmed(){
		
		return centreServiceLocal.findConfirmedCentre();
	}
	
//***********************************************************************************************************************
	
	
	
	
	
	
	
	
	
	
	



	
	public List<CentreDoc> getCentres() {
		return centres;
	}

	public void setCentres(List<CentreDoc> centres) {
		this.centres = centres;
	}

	public CentreDoc getCentredoc() {
		return centredoc;
	}

	public void setCentredoc(CentreDoc centredoc) {
		this.centredoc = centredoc;
	}
	
	public CentreDoc getCentredoc1() {
		return centredoc1;
	}

	public void setCentredoc1(CentreDoc centredoc1) {
		this.centredoc1 = centredoc1;
	}

	public CentreDoc getCentredoc2() {
		return centredoc2;
	}

	public void setCentredoc2(CentreDoc centredoc2) {
		this.centredoc2 = centredoc2;
	}

	public static String getNOMC() {
		return NOMC;
	}

	public static void setNOMC(String nOMC) {
		NOMC = nOMC;
	}

	public Localisation getLocal() {
		return local;
	}

	public void setLocal(Localisation local) {
		this.local = local;
	}

	public static String getPays() {
		return pays;
	}

	public static void setPays(String pays) {
		CentreDocBean.pays = pays;
	}

	public static String getVille() {
		return ville;
	}

	public static void setVille(String ville) {
		CentreDocBean.ville = ville;
	}

	public static String getType() {
		return type;
	}

	public static void setType(String type) {
		CentreDocBean.type = type;
	}

	public static int getRecom() {
		return recom;
	}

	public static void setRecom(int recom) {
		CentreDocBean.recom = recom;
	}

	public MapModel getSimpleModel() {
        return simpleModel;
    }
	
	
	
}
