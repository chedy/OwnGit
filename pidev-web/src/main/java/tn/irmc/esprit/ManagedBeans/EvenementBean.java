package tn.irmc.esprit.ManagedBeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;

import tn.irmc.esprit.entities.CurrentUser;
import tn.irmc.esprit.entities.Evenement;
import tn.irmc.esprit.services.EvenementServiceLocal;
import tn.irmc.esprit.services.UserServiceRemote;

@ManagedBean
// @SessionScoped
@RequestScoped
public class EvenementBean implements Serializable {

	/**
	 * 
	 */

	private static final long serialVersionUID = 1L;
	@EJB
	private EvenementServiceLocal evenementServiceLocal;
    @EJB
    private UserServiceRemote userService ; 
    
	private Evenement e;
	private List<Evenement> events;
	private List<Evenement>myevent = new ArrayList<>();
	private boolean showDetails = false;
	private Date date;

	public EvenementBean() {

		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void Init() {
		e=new Evenement();
		events = evenementServiceLocal.findAllEvents();
		myevent=evenementServiceLocal.findAllEvents();
		date = new Date();

	}

	public String doAddEvent() {
		String navTo = "/pages/User/EventsList?faces-redirect=true";
		
		e.setUser(userService.findById(AuthenticationBean.getLoggedUser().CurrentUserId));
		evenementServiceLocal.addEvent(e);
		Init();
		return navTo;

	}
	
	public void doSelect() {
		//String navTo="/pages/User/MyListEvent?faces-redirect=true";
		showDetails = true;
		//return navTo;
	}
	public String edit(Evenement ee)
	{
		this.e=ee;
		return "editEvent";
	}
	public String edit()
	{
		this.e.setUser(evenementServiceLocal.findByIdUser(CurrentUser.CurrentUserId));
		//this.e.setNom("kaisser");
		this.e.setNom(e.getNom());
		this.e.setDateDebut(e.getDateDebut());
		this.e.setDateFin(e.getDateFin());
		this.e.setTel(e.getTel());
		this.e.setNbrPlace(e.getNbrPlace());
		this.e.setLieux(e.getLieux());
		this.e.setMap(e.getMap());
		this.e.setType(e.getType());
		this.e.setDescription(e.getDescription());
		this.evenementServiceLocal.updateEvent(this.e);
		//Init();
		return "/pages/User/EventsList?faces-redirect=true";
	}
	public String doDelete(){
		String navTo = "/pages/User/EventsList?faces-redirect=true";
		evenementServiceLocal.removeEvent(e);
		events = evenementServiceLocal.findAllEvents();
		//Init();
		return navTo;
	}


	/*public Evenement Show() {
		return evenementServiceLocal.findById(EvenementService.ESL);
	}

	
	public String mettreajourEvent() {

		this.evenementServiceLocal.updateEvent(this.evenement1);
		return "/pages/ListEvent?faces-redirect=true";
	}

	public String doEdit() {
		String navTo = "/pages/editEvents?faces-redirect=true";
		// this.evenement1.setUser(evenementServiceLocal.findByIdUser(CurrentUser.CurrentUserId));
		this.evenement1.setNom(evenement1.getNom());
		return navTo;
	}

	public String doDelete() {
		String navTo = "/pages/User/ListEvent?faces-redirect=true";
		evenementServiceLocal.removeEvent(evenement1);
		Init();
		return navTo;
	}

	public String doSelect(Evenement evenement1) {
		String navTo = "/pages/User/DetaillsEvent?faces-redirect=true";
		EvenementService.ESL = evenement1.getIdEvent();

		return navTo;

	}*/

	public List<Evenement> getEvents() {
		return events;
	}

	public void setEvents(List<Evenement> events) {
		this.events = events;
	}

	

	public boolean isShowDetails() {
		return showDetails;
	}

	public void setShowDetails(boolean showDetails) {
		this.showDetails = showDetails;
	}

	public Evenement getE() {
		return e;
	}

	public void setE(Evenement e) {
		this.e = e;
	}

	public List<Evenement> getMyevent() {
		return myevent;
	}

	public void setMyevent(List<Evenement> myevent) {
		this.myevent = myevent;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	


	

	/*public Evenement getEvenement1() {
		return evenement1;
	}

	public void setEvenement1(Evenement evenement1) {
		this.evenement1 = evenement1;
	}*/

}
