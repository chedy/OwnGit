package tn.irmc.esprit.ManagedBeans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import tn.irmc.esprit.entities.Category;
import tn.irmc.esprit.entities.CurrentUser;
import tn.irmc.esprit.entities.Document;
import tn.irmc.esprit.entities.Evenement;
import tn.irmc.esprit.services.DocumentServiceRemote;
import tn.irmc.esprit.services.EvenementServiceLocal;
@ManagedBean
@RequestScoped
public class DocumentBean implements Serializable{
    
    @EJB
	private DocumentServiceRemote docService;
    
    private Document document ; 
    private List<Document> lstDocs; 
    private List<Document> lstTopRated ; 
    private static List<Document> lstByCategory ; 
    private List<Document> lstByDate ; 
    private List<Document> lstMyDocs ; 
 
    private static Document DocDetails ; 
    private static int idToUpdate; 
    private String category ; 
    private boolean formDisplayed=false;
    private String motCle ; 
    private static List<Document> lstSearch ; 
    
    

    
    
	public Document getDocument() {
		return document;
	}
	public void setDocument(Document document) {
		this.document = document;
	}
	public List<Document> getLstDocs() {
		lstDocs =  docService.getAllDocuments() ;
		return lstDocs ;
	}
	public void setLstDocs(List<Document> lstDocs) {
		this.lstDocs = lstDocs;
	}
	public boolean isFormDisplayed() {
		return formDisplayed;
	}
	public void setFormDisplayed(boolean formDisplayed) {
		this.formDisplayed = formDisplayed;
	}
	
	
	public DocumentBean(List<Document> lstDocs) {
		super();
		this.lstDocs = lstDocs;
	}
	public DocumentBean() {
		super();
		// TODO Auto-generated constructor stub
	}
	@PostConstruct
	public void Init(){
		document= new Document();
		document.setPostDate(new Date());
		document.setEtat(false);
		document.setRate(0);
		document.setPieceJointe(null);
		document.setImage(null);
		lstDocs = docService.getAllDocuments();
		lstTopRated = docService.getTopRated(); 
		lstMyDocs = docService.getMyDocument(2);
		lstByDate = docService.getByDate();
		
		
		
		
		
		
	}
	
	public String doAddDoc(){
		String navTo = "/pages/User/ListDocuments?faces-redirect=true";
		docService.addDocument(document);
		
		return navTo ;
	}
	
	public String doRate(int id){
		String navTo = "/pages/User/ListDocuments?faces-redirect=true"; 
		docService.rateDocument(id);
		return navTo ; 
	}
	
	public String doSearchByCategory(Category cat){
		
		String navTo = "/pages/User/DocByCategory?faces-redirect=true";
        System.out.println(cat);
		lstByCategory = docService.getDocumentsByCategory(cat);
		System.out.println(lstByCategory.toString());
		return navTo ;
	}
	public String doSearch(String motCle){
		 String navTo = "/pages/User/DocSearch?faces-redirect=true";
		 lstSearch = docService.search(motCle) ; 
		 return navTo ; 
	}
	
	public  List<Document> getLstSearch() {
		return lstSearch;
	}
	public  void setLstSearch(List<Document> lstSearch) {
		DocumentBean.lstSearch = lstSearch;
	}
	public String getDetails(int idDoc){
		this.setDocDetails(docService.getDetails(idDoc));
		
		System.out.println(DocDetails.toString());
		
		String navTo = "/pages/User/DocDetails?faces-redirect=true";
		
		return navTo ; 
	}
	
	
	public String doDelete(int id){
	
	    String navTo ; 	
		if( docService.deleteDocument(id) == true ) ; 
		{
			 navTo =  "/pages/User/ListDocuments?faces-redirect=true";
		}
		navTo = "/pages/User/Error?faces-redirect=true";
		return navTo ; 	
		
	}
	
	public String edit(Document doc)
	{
		
		
		this.document.setName(doc.getName());
		this.document.setDescription(doc.getDescription());
		this.setIdToUpdate(doc.getId());
		System.out.println(idToUpdate);
		return "/pages/User/ediDocument?faces-redirect=true";
	}
	public String DoEdit()
	{
		

		this.docService.updateDocument(new Document(idToUpdate,document.getName(), document.getDescription()));
		return "/pages/User/ListDocuments?faces-redirect=true";
	}
	
	public List<Document> getLstTopRated() {
		return lstTopRated;
	}
	public void setLstTopRated(List<Document> lstTopRated) {
		this.lstTopRated = lstTopRated;
	}
	public List<Document> getLstByCategory() {
		return lstByCategory;
	}
	public void setLstByCategory(List<Document> lstByCategory) {
		this.lstByCategory = lstByCategory;
	}
	public List<Document> getLstByDate() {
		return lstByDate;
	}
	public void setLstByDate(List<Document> lstByDate) {
		this.lstByDate = lstByDate;
	}
	public List<Document> getLstMyDocs() {
		return lstMyDocs;
	}
	public void setLstMyDocs(List<Document> lstMyDocs) {
		this.lstMyDocs = lstMyDocs;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Document getDocDetails() {
		return DocDetails;
	}
	public void setDocDetails(Document docDetails) {
		DocDetails = docDetails;
	}
	public int getIdToUpdate() {
		return idToUpdate;
	}
	public void setIdToUpdate(int idToUpdate) {
		this.idToUpdate = idToUpdate;
	}
	public String getMotCle() {
		return motCle;
	}
	public void setMotCle(String motCle) {
		this.motCle = motCle;
	}
	
	
	
	
	
	
    
    
    
    
}
