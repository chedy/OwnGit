package tn.irmc.esprit.ManagedBeans;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;



import tn.irmc.esprit.entities.CurrentUser;
import tn.irmc.esprit.entities.Role;
import tn.irmc.esprit.entities.User;
import tn.irmc.esprit.services.UserServiceLocal;

@ManagedBean(name = "authBean")
@SessionScoped
public class AuthenticationBean implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@EJB
	private UserServiceLocal userServiceLocal;
	private User user;
	private boolean loggedIn;
	private static CurrentUser LoggedUser;
	
	public AuthenticationBean() {
		super();
	}
	@PostConstruct
	public void initModel() {
		user = new User();
		loggedIn = false;
	}
	
	public String doLogin() {
		String navigateTo = null;
		// login application logic
		
		User found = userServiceLocal.authenticate(user.getLogin(),
				user.getPassword());
		if (found != null) {
			user = found;
			loggedIn = true;
			
			if (user.getRole().toString() == Role.Admin.toString()) {
				navigateTo = "/pages/admin/Dashboard?faces-redirect=true";
				LoggedUser.CurrentUserId=user.getId();
				LoggedUser.userName=user.getLogin();
			}
			if (user.getRole().toString() == Role.User.toString()) {
				navigateTo = "/pages/User/Userhome?faces-redirect=true";
				LoggedUser.CurrentUserId=user.getId();
				LoggedUser.userName=user.getLogin();
			}

		} else {
			FacesContext.getCurrentInstance().addMessage(
					"login_form:login_submit",
					new FacesMessage("Bad credentials!"));
		}
		return navigateTo;
	}
	
	public String doLogOut() {
		String navigateTo = null;
		initModel();
		
		navigateTo = "/welcome?faces-redirect=true";
		return navigateTo;
	}
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public boolean isLoggedIn() {
		return loggedIn;
	}
	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	public static CurrentUser getLoggedUser() {
		return LoggedUser;
	}
	public static void setLoggedUser(CurrentUser loggedUser) {
		LoggedUser = loggedUser;
	}
	
	

	
}
