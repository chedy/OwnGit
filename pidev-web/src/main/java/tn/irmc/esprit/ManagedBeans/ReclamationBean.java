package tn.irmc.esprit.ManagedBeans;

import java.io.Serializable;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.naming.NamingException;

import tn.irmc.esprit.entities.Offre;
import tn.irmc.esprit.entities.Reclamation;
import tn.irmc.esprit.entities.sendmail;
import tn.irmc.esprit.services.OfferServiceLocal;
import tn.irmc.esprit.services.ReclamationServiceLocal;

@ManagedBean(name = "reclamationbean")
public class ReclamationBean implements Serializable {

	/**
	 * 
	 */
	@EJB
	private ReclamationServiceLocal reclamationService;
	@EJB
	private OfferServiceLocal serviceOffer;
	@EJB
	sendmail mailSender;
	private static final long serialVersionUID = 1L;
	private Reclamation rec;
	private Offre off;
	private Date date;
	private static int idoff;
	private static int iduser;
	private List<Reclamation> Reclamations = new ArrayList<>();

	public ReclamationBean() {
		// TODO Auto-generated constructor stub
	}

	@PostConstruct
	public void init() {
		off = new Offre();
		rec = new Reclamation();
		date = new Date();
		Reclamations = reclamationService.findAllRec();
	}

	public String doAccepter() throws AddressException, MessagingException, NamingException {
		off = serviceOffer.findById(idoff);
		rec = reclamationService.findById(iduser);
		System.out.println(off.getReference());
		String mailOffre = off.getUser().getEmail();
		System.out.println(mailOffre);
		String mailReclamation = rec.getUser().getEmail();
		System.out.println(mailReclamation);
		mailSender.sendMail(
				"Nous tenons à vous informer que votre réclamation envoyer le " + rec.getDateRec()
						+ " a été prise en compte "
						+ "Nous restons à votre entière disposition pour toute question ou suggestion.\n L'équipe du Service Clients IRMC.",
				mailReclamation, "Reclamation IRMC");
		mailSender.sendMail(
				"Votre publication intitulé " + off.getTitre() + " publier le " + off.getDatePublication()
						+ " a été supprimer par l'administration suite à une reclamation.\n L'équipe du Service Clients IRMC.",
				mailOffre, "Reclamation IRMC");
		this.serviceOffer.deleteOffre(off);

		init();
		return "/pages/admin/listReclamation?faces-redirect=true";
	}

	public String doRefuser() throws AddressException, MessagingException, NamingException {

		rec = reclamationService.findById(iduser);
		String mailReclamation = rec.getUser().getEmail();
		System.out.println(mailReclamation);
		mailSender.sendMail(
				"Nous tenons à vous informer que votre réclamation envoyer le " + rec.getDateRec()
						+ " a été prise en compte "
						+ "Nous restons à votre entière disposition pour toute question ou suggestion.\n L'équipe du Service Clients IRMC.",
				mailReclamation, "Reclamation IRMC");
		reclamationService.deleteReclamation(rec);
		init();
		return "/pages/admin/listReclamation?faces-redirect=true";
	}

	public String doSelectOffre() {

		this.off = serviceOffer.findById(idoff);
		System.out.println(this.off.getReference());
		return "detailsoff";
	}

	public String doSelect(Reclamation e) {
		this.rec = e;
		idoff = e.getOffre().getId();
		iduser = e.getIdReclamation();
		return "afficherReclamation";
	}

	public String doAddRec() throws ParseException {
		String navTo = "/pages/Home?faces-redirect=true";
		rec.setDateRec(date);
		rec.setOffre(serviceOffer.findById(proposeOfferBean.id));
		reclamationService.addReclamation(rec);
		init();
		return navTo;
	}

	public String doSelect() {
		return "/pages/User/reclamation?faces-redirect=true";
	}

	public Reclamation getRec() {
		return rec;
	}

	public void setRec(Reclamation rec) {
		this.rec = rec;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public static int getIdoff() {
		return idoff;
	}

	public static void setIdoff(int idoff) {
		ReclamationBean.idoff = idoff;
	}

	public List<Reclamation> getReclamations() {
		return Reclamations;
	}

	public void setReclamations(List<Reclamation> reclamations) {
		Reclamations = reclamations;
	}

	public Offre getOff() {
		return off;
	}

	public void setOff(Offre off) {
		this.off = off;
	}

}
